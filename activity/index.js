// console.log("Hi");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	
function displayDetails(){
  let yourFullName = prompt ("Input your full name");
  let yourAge = prompt ("Input Age");
  let yourLocation = prompt ("Input your location");

  alert("hi "+ yourFullName + ",thank you for your input" );

  console.log("Full Name: " + yourFullName);
  console.log("Age: " + yourAge);
  console.log("Location: " + yourLocation);



};

displayDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function displayMyFavoriteBands(){
  let myFavoriteBand = ["Neocolours","Eraserheads","Parokya ni Edgar","Rivermaya", "Kamikazee"];

  console.log("These are my favorite Filipino OPM Bands: ");

  console.log("1. " + myFavoriteBand[0]);
  console.log("2. " + myFavoriteBand[1]);
  console.log("3. " + myFavoriteBand[2]);
  console.log("4. " + myFavoriteBand[3]);
  console.log("5. " + myFavoriteBand[4]);
  

}

displayMyFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function displayMyFavoriteMovies(){
  let myFavoriteMovies = ["Jumper", "The Mummy","Angels & Demons","2012","Transporter"];
  let myFavoriteMoviesRating = ["15%","17%","37%","39%","40%"]
  
  console.log("These are my favorite movies along with their tomatometer from rotten tomato: ");

  console.log("1. " + myFavoriteMovies[0]);
  console.log(" Rotten Tomato Rating:  " + myFavoriteMoviesRating[0]);

  console.log("2. " + myFavoriteMovies[1]);
  console.log(" Rotten Tomato Rating:  " + myFavoriteMoviesRating[1]);

  console.log("3. " + myFavoriteMovies[2]);
  console.log(" Rotten Tomato Rating:  " + myFavoriteMoviesRating[2]);

  console.log("4. " + myFavoriteMovies[3]);
  console.log(" Rotten Tomato Rating:  " + myFavoriteMoviesRating[3]);

  console.log("5. " + myFavoriteMovies[4]);
  console.log(" Rotten Tomato Rating:  " + myFavoriteMoviesRating[4]);
}

displayMyFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
